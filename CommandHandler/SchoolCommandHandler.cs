﻿using Aggregate;
using AggregateIdentity;
using Command;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;
using System.Threading;
using System.Threading.Tasks;

namespace CommandHandler
{
    public class SchoolCommandHandler :
        CommandHandler<SchoolAggregate, SchoolId, IExecutionResult, SchoolCommand>
    {
        public override Task<IExecutionResult> ExecuteCommandAsync(
            SchoolAggregate aggregate,
            SchoolCommand command,
            CancellationToken cancellationToken)
        {
            var executionResult = aggregate.SetMagicNumer(command.MagicNumber);
            return Task.FromResult(executionResult);
        }
    }
}