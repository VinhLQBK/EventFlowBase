﻿using Aggregate;
using AggregateIdentity;
using EventFlow.Aggregates.ExecutionResults;
using EventFlow.Commands;

namespace Command
{
    public class UserCommand :
        Command<UserAggregate, UserId, IExecutionResult>
    {
        public UserCommand(
            UserId aggregateId,
            int magicNumber)
            : base(aggregateId)
        {
            MagicNumber = magicNumber;
        }

        public int MagicNumber { get; }
    }
}