﻿using EventFlow.Core;

namespace AggregateIdentity
{
    public class UserId :
        Identity<UserId>
    {
        public UserId(string value) : base(value) { }
    }
}