﻿using AggregateIdentity;
using EventFlow.Aggregates;
using EventFlow.Aggregates.ExecutionResults;

namespace Aggregate
{
    public class SchoolAggregate :
        AggregateRoot<SchoolAggregate, SchoolId>,
        IEmit<SchoolEvent>
    {
        private int? _magicNumber;

        public SchoolAggregate(SchoolId id) : base(id) { }

        // Method invoked by our command
        public IExecutionResult SetMagicNumer(int magicNumber)
        {
            if (_magicNumber.HasValue)
                return ExecutionResult.Failed("Magic number already set");

            Emit(new SchoolEvent(magicNumber));

            return ExecutionResult.Success();
        }

        // We apply the event as part of the event sourcing system. EventFlow
        // provides several different methods for doing this, e.g. state objects,
        // the Apply method is merely the simplest
        public void Apply(SchoolEvent aggregateEvent)
        {
            _magicNumber = aggregateEvent.MagicNumber;
        }
    }
}
