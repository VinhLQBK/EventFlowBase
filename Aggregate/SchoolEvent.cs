﻿using Aggregate;
using AggregateIdentity;
using EventFlow.Aggregates;
using EventFlow.EventStores;

namespace Aggregate
{
    [EventVersion("example", 1)]
    public class SchoolEvent :
        AggregateEvent<SchoolAggregate, SchoolId>
    {
        public SchoolEvent(int magicNumber)
        {
            MagicNumber = magicNumber;
        }

        public int MagicNumber { get; }
    }
}