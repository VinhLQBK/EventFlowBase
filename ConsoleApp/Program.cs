﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Aggregate;
using AggregateIdentity;
using Command;
using CommandHandler;
using EventFlow;
using EventFlow.Extensions;
using EventFlow.MsSql;
using EventFlow.MsSql.EventStores;
using EventFlow.MsSql.Extensions;
using EventFlow.Queries;
using EventFlow.RabbitMQ;
using EventFlow.RabbitMQ.Extensions;
using EventHandler;
using FluentAssertions;
using NUnit.Framework;
using ReadModels;

namespace ConsoleApp
{
    class Program
    {
        static async Task Main()
        {
            using (var resolver = EventFlowOptions.New
                .PublishToRabbitMq(RabbitMqConfiguration.With(new Uri("amqp://localhost")))
                .ConfigureMsSql(MsSqlConfiguration.New
                .SetConnectionString(@"Server=VINHLQ\SQLEXPRESS;Database=EventFlowBase;Integrated Security=True"))
                .UseMssqlEventStore()
                .AddEvents(typeof(UserEvent))
                .AddCommands(typeof(UserCommand))
                .AddCommandHandlers(typeof(UserCommandHandler))
                .UseInMemoryReadStoreFor<UserReadModel>()
                .AddEvents(typeof(SchoolEvent))
                .AddCommands(typeof(SchoolCommand))
                .AddCommandHandlers(typeof(SchoolCommandHandler))
                .AddSynchronousSubscriber<UserAggregate, UserId, UserEvent, UserEventHandler>()
                //.UseMssqlReadModel<UserReadModel>()
                .CreateResolver())
            {
                #region Init sql

                //var msSqlDatabaseMigrator = resolver.Resolve<IMsSqlDatabaseMigrator>();
                //EventFlowEventStoresMsSql.MigrateDatabase(msSqlDatabaseMigrator);

                #endregion

                #region User

                // Create a new identity for our aggregate root
                // Create a namespace, put this in a constant somewhere
                var emailNamespace = Guid.Parse("769077C6-F84D-46E3-AD2E-828A576AAAF3");

                // Creates an identity with the value "test-9181a444-af25-567e-a866-c263b6f6119a"
                var exampleId = UserId.NewDeterministic(emailNamespace, "test@example.com");
                //var exampleId = UserId.New;

                // Define some important value
                const int magicNumber = 44;

                // Resolve the command bus and use it to publish a command
                var commandBus = resolver.Resolve<ICommandBus>();
                var executionResult = await commandBus.PublishAsync(
                    new UserCommand(exampleId, magicNumber),
                    CancellationToken.None)
                    .ConfigureAwait(false);

                // Verify that we didn't trigger our domain validation
                executionResult.IsSuccess.Should().BeTrue();

                // Resolve the query handler and use the built-in query for fetching
                // read models by identity to get our read model representing the
                // state of our aggregate root
                var queryProcessor = resolver.Resolve<IQueryProcessor>();
                var exampleReadModel = await queryProcessor.ProcessAsync(
                    new ReadModelByIdQuery<UserReadModel>(exampleId),
                    CancellationToken.None)
                    .ConfigureAwait(false);

                // Verify that the read model has the expected magic number
                exampleReadModel.MagicNumber.Should().Be(44);

                #endregion
            }
        }
    }
}
